const path = require('path');
const webpack = require('webpack');
const mix = require('laravel-mix');

if (process.env.section) {
  require(`${__dirname}/webpack.mix.${process.env.section}.js`);
}

mix.webpackConfig({
  plugins: [
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/)
  ],
  resolve: {
    extensions: ['.js', '.json', '.vue'],
    alias: {
      '~': path.join(__dirname, './js')
    }
  },
  output: {
    chunkFilename: 'assets/[chunkhash].js',
    path: mix.config.hmr ? '/' : path.resolve(__dirname, './app/'),
  },
});

mix.js('./js/app.js', 'assets/app.js');
