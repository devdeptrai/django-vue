import store from './../store'

export default async (to, from, next) => {
  try {
    const name = to.name;
    let token = store.getters['auth/token_user'];

    if (!token) {
      if (name.includes('login')) {
        next()
      } else {
        next({ name: 'login' })
      }
    } else {
      if (name.includes('login')) {
        next({ name: 'index' })
      } else {
        await store.dispatch('auth/fetchUser');
        next()
      }
    } 
  } catch (e) { }

  next()
}
