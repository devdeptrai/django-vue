import Vue from 'vue'
import store from './store'
import router from './router'
import i18n from './plugins/i18n'
import App from './components/App.vue'

import DefaultLayout from "./layouts/DefaultLayout.vue";
import LoginLayout from "./layouts/LoginLayout.vue";

import './plugins'

Vue.component("DefaultLayout", DefaultLayout);
Vue.component("LoginLayout", LoginLayout);

/* eslint-disable no-new */
new Vue({
  i18n,
  store,
  router,
  ...App
});
