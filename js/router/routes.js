function page (path) {
  return () => import(/* webpackChunkName: '' */ `../pages/${path}`).then(m => m.default || m)
}

export default [
  {
    path: 'login',
    name: 'login',
    component: page('login.vue'),
    meta: {
      layout: 'LoginLayout'
    }
  },
  {
    path: '/',
    component: {
      render(c) {
        return c('router-view')
      }
    },
    meta: {
      layout: "DefaultLayout",
      guest: true
    },
    name: 'index',
    children: [
      {
        path: '',
        component: page('index.vue')
      }
    ]
  }
]
