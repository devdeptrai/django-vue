import axios from 'axios'
import Cookies from 'js-cookie'
import * as types from '../mutation-types'

// state
export const state = {
  user: null,
  token_user: Cookies.get('token_user'),
}

// getters
export const getters = {
  user: state => state.user,
  token_user: state => state.token_user,
}

// mutations
export const mutations = {
  [types.SAVE_TOKEN_USER] (state, { token, remember }) {
    state.token_user = token;
    Cookies.set('token_user', token, { expires: remember ? 365 : null })
  },
  [types.FETCH_USER_SUCCESS] (state, { user }) {
    state.user = user
  },
  [types.FETCH_USER_FAILURE] (state) {
    state.token_user = null
    Cookies.remove('token_user')
  },
  [types.LOGOUT_USER] (state) {
    state.user = null
    state.token_user = null
    Cookies.remove('token_user')
  },
  login(state) {
    state.token_user = 'login_token'
  }
}

// actions
export const actions = {
  saveTokenUser ({ commit, dispatch }, payload) {
    commit(types.SAVE_TOKEN_USER, payload)
  },
  async fetchUser ({ commit }) {
    try {
      const { data } = await axios.get('/api/user')
      commit(types.FETCH_USER_SUCCESS, { user: data })
    } catch (e) {
      commit(types.FETCH_USER_FAILURE)
    }
  },

  async logout ({ commit }) {
    try {
    } catch (e) { }

    commit(types.LOGOUT_USER)
  },

  async login({commit}) {
    commit('login')
  }
}
