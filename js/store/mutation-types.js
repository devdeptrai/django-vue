// auth.js
export const LOGOUT_USER = 'LOGOUT_USER'

export const SAVE_TOKEN_USER = 'SAVE_TOKEN_USER'
export const FETCH_USER = 'FETCH_USER'
export const FETCH_USER_SUCCESS = 'FETCH_USER_SUCCESS'
export const FETCH_USER_FAILURE = 'FETCH_USER_FAILURE'

// lang.js
export const SET_LOCALE = 'SET_LOCALE'

